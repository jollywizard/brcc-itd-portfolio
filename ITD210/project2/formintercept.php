<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Discover Greece - The Beauty of the Greek Landscape</title>
        <link href="styles.css" rel="stylesheet" type="text/css" />
        <link href="upper2.css" rel="stylesheet" type="text/css" />
        <link href="lower.css" rel="stylesheet" type="text/css" />
        <link href="forms.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="columnheight.js"></script>
        <script type="text/javascript" src="Tools.js"></script>

    </head>

    <body  onload="init(); ">

        <div id="wrapper">

            <div id="header">
                <img src="images/medusa.gif" alt ="medusa" id="medusa"/>
                <h1>DISCOVER GREECE</h1>
            </div>

            <div id="innerwrapper" >
                <div id="nav">
                    <ul>
                        <li> <a href="index.html">HOME</a></li>
                        <li class="bullet" />
                        <li> <a href="people.html">PEOPLE</a></li>
                        <li class="bullet" />
                        <li> <a href="history.html">HISTORY</a></li>
                        <li class="bullet" />
                        <li> <a href="archaeology.html">ARCHAEOLOGY</a></li>
                        <li class="bullet" />
                        <li> <a href="nature.html">NATURE</a></li>
                    </ul>

                </div>

                <div class="upper">

                    <!-- The big box with Zeus -->
                    <div class="content"   >
                        <img src="images/dirtroad.jpg"
                             alt="Is it zeus? or poseidon? we can't tell because
                             we're not properly educated"
                             />

                        <div id="largeIntroDescription">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum molestie interdum diam at
                            semper. Nam elit ipsum, tempus quis viverra nec, adipiscing eget libero. Aenean ut risus et
                            diam ullamcorper adipiscing. Curabitur tristique interdum lorem, eget tempor odio iaculis in.
                        </div>

                    </div>
                    <!-- END big box with Zeus -->
                    <div class="content right">
                        <h2>Natural Wonders of Greece</h2>
                        <div class="menu">
                            <ul>
                                <li><a href="nature.html">MAIN</a></li>
                                <li>/</li>
                                <li><a href="#">COASTS</a></li>
                                <li>/</li>
                                <li><a href="#">MOUNTAINS</a></li>
                                <li>/</li>
                                <li><a href="#">ISLANDS</a></li>
                            </ul>
                        </div>
                        <div class="quote">
                            <p>
                                How 'bout I run away then mom.  How would you feel about that?</p>
                            <div class="credentials author">Stratos Argyriou</div>
                            <div class="credentials">Childhood Friend</div>
                        </div>
                        <div class="quote">
                            <p>
                                Don't be a stupid boy.  I'll cut off your feet.</p>
                            <div class="credentials author">Eve Argyriou</div>
                            <div class="credentials">Greek Mother</div>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum molestie interdum diam at
                            semper. Nam elit ipsum, tempus quis viverra nec, adipiscing eget libero. Aenean ut risus et
                            diam ullamcorper adipiscing. Curabitur tristique interdum lorem, eget tempor odio iaculis in.
                            <a href="#" class="more">MORE &gt;&gt;</a>

                        </p>
                    </div>


                </div>

                <div id="lowerSpacer" ></div>

                <div id="lowerContent">

                    <div id="lowerContentArticles" >

                        <div class="header">
                            <div class="right">July 12th, 2010</div>
                            LATEST NEWS
                        </div>

                        <div class="last article">
                            <?php echo("<br/><p>Thanks for your info sucka! Now we're going to steal your identity</p>");?>
                        </div>
                    </div>

                    <div id="lowerContentSidebar">
                        <div class="header">
                            TRAVEL
                        </div>
                        <img src="images/tour2010.gif"
                             alt="A fancy tour greece poster for people with
                             fancy pants."
                             />
                    </div>

                </div>

                <div id="footer">
        	Copyright &copy; 2010 - Discover Greece!
                </div>
            </div >
        </div>
    </body>
</html>
