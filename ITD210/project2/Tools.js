
/**
 *Acts as a central initializer to page load functions
*/
init=function() {
    matchHeight();
    goldLink();
}

/**
 * Finds hrefs in the two menus that are part of the current location and turns
 * them gold.
 */

goldLink=function() {

    //First load the nav structure
    var nav;
    nav = document.getElementById("nav");
    
    //then grab the navigation loop
    var ul;
    for (var i = 0; i <nav.childNodes.length; i++) {
        ul = nav.childNodes[i];
        if (ul instanceof HTMLUListElement) break;
    }

    //load the li elements into an array
    var lis = [];
    for (i = 0; i < ul.childNodes.length; i++) {
        if (ul.childNodes[i] instanceof HTMLLIElement) {
            // alert(i + "="+ul.childNodes[i]);
            lis[lis.length] = ul.childNodes[i];
        }
    }

    //Set active main menu links and cache in as
    var as = [];
    var li;
    for (i = 0; i < lis.length; i++) {
        li = lis[i];
        for (var j = 0; j < li.childNodes.length; j++) {
            //alert(li.childNodes[j]);
            if (li.childNodes[j] instanceof HTMLAnchorElement) {
                as[as.length] = li.childNodes[j];
                if (li.childNodes[j].href == location.href) {
                    li.childNodes[j].style.color = "#C26E22";
                } //else alert("No Match: " + li.childNodes[j].href)
            }
        }
    }

    /**
     * find the content menu and cached its li's in inLis (inner Li's)
     */
    var divs, menu, inLis, inAs = [];
    divs = document.getElementsByTagName('div');
    for (i = 0; i < divs.length; i++){
        if(/\bmenu\b/.test(divs[i].className)) {
            menu = divs[i];
            break;
        }
    }
    //if the menu was found
    if (menu != null) {
        try {
            // cached the lis
            inLis = menu.getElementsByTagName('ul')[0].getElementsByTagName('li');

            // for each
            for (i = 0; i < inLis.length; i++) {
                li = inLis[i];
                //if its an anchor li add it to the list
                if (/a/.test(li.innerHTML)) {
                    inAs[inAs.length] = li.getElementsByTagName('a')[0];
                }
            }

           // alert(as.length);
            //for all cached anchors
            for (i = 0; i < inAs.length; i++) {
                // alert(inAs[i])
                //if the location is the same as current turn gold
                if (inAs[i].href == location.href) {
                    inAs[i].style.color = "#C26E22";
                }

                /*
                 * if one of the main menu's location is the same,
                 * then we are in a sub-page so highlight the menu.
                 */
                for (j = 0; j < as.length; j++) {
                    if (as[j].href == inAs[i].href) {
                    //    alert("Match: " + as[j]);
                        as[j].style.color = "#C26E22";
                    }
                }
            }

        } catch (e) {
            //since I'm bad at javascript tell me what i did wrong
            alert(e);
        }
    }
}