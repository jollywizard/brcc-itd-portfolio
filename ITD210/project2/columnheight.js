/**
 * Matching div heights with CSS and JavaScript
*  By: Alejandro Gervasio
*
*  Pulled From : http://www.devarticles.com/c/a/Web-Design-Standards/Matching-div-heights-with-CSS-and-JavaScript/3/
*
*  I was going to do this myself, but its only 20 lines of code,
*  so I couldn't honestly claim originality now that I've seen it.
*
*  I did modify it to work in Firefox by concatinating "px" to the end of
*  the call to set style height.  Apparently, the author was unaware that IE is
*  the only browser to accept literal numbers as px.
*
 */
matchHeight=function(){

    var divs,contDivs,maxHeight,divHeight,d;

    // get all <div> elements in the document

    divs=document.getElementsByTagName('div');

    contDivs=[];

    // initialize maximum height value

    maxHeight=0;

    // iterate over all <div> elements in the document

    for(var i=0;i<divs.length;i++){

        // make collection with <div> elements with class attribute 'content'

        //added my tag here
        if(/\bcontent\b/.test(divs[i].className)){

            d=divs[i];

            contDivs[contDivs.length]=d;

            // determine height for <div> element

            if(d.offsetHeight){

                divHeight=d.offsetHeight;
             

            }

            else if(d.style.pixelHeight){

                divHeight=d.style.pixelHeight;
       
            }

            // calculate maximum height

            maxHeight=Math.max(maxHeight,divHeight);
        
        }  

    }



    // assign maximum height value to all of container <div> elements

    for(var i=0; i<contDivs.length;i++){
 
        contDivs[i].style.height=maxHeight + "px";
     
    }

}

// execute function when page loads

window.onload=function(){

    if(document.getElementsByTagName){

        matchHeight();

    }

}