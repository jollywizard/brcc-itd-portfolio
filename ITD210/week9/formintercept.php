<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Form Intercept Page</title>
</head>

<body>

<?
//DO NOT CHANGE ANYTHING WITHIN THIS BLOCK OF CODE

echo("Thank you for your information!" );


//There's very little error correction here.
echo("<br />");
echo("First name: " . $_POST["txtFirstName"]);
echo("<br />");
echo("Last name: " . $_POST["txtLastName"]);
echo("<br />");
echo("Email: " . $_POST["txtEmail"]);
echo("<br />");
echo("Address 1: " . $_POST["txtAddress1"]);
echo("<br />");
echo("Address 2: " . $_POST["txtAddress2"]);
echo("<br />");
echo("City: " . $_POST["txtCity"]);
echo("<br />");
echo("State: " . $_POST["selState"]);
echo("<br />");
echo("Zip: " . $_POST["txtZip"]);
echo("<br />");

if( $_POST["chkAddToList"] == "on" )
{
	echo("Add to mailing list: Yes");
}
else
{
	echo("Add to mailing list: No");
}
echo("<br />");
if( $_POST["chkAddToAffiliates"] == "on" )
{
	echo("Add to affiliates: Yes");
}
else
{
	echo("Add to affiliates: No");
}

echo("<br />");

$payment = $_POST["radPayment"];
if( $payment == "Visa" )
{
	echo("You chose to use Visa");
}
else if( $payment == "MasterCard" )
{
	echo("You chose to use MasterCard" );
}
else if( $payment == "AmEx" )
{
	echo("You chose to use American Express" );
}
else
{
	echo("You didn't choose a payment method, or the values you used in your input tags weren't Visa, MasterCard or AmEx"); 
}
echo("<br />");


?>

</body>
</html>
