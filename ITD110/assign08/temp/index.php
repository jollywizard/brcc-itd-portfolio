<?php
//the xml header has to be explicitly echoed or it is mistaken for a php tag.echo
'<?xml version="1.0" encoding="utf-8"?>';

include "functions.php"; //all php functions are stored in external file.
?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- The data and color scales for table1 are generated using an external
php file, functions.php, located in the same directory.-->
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Week 8 Advanced Tables - James Arlow</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style type="text/css">@import url("styles.css")</style>
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
    </head>
    <body>
        <h1>Week 8 Advanced Tables - James Arlow</h1>
        <h2 id="weather">Weather Table</h2>
        <table class="weatherTable">
            <?php colums(13); ?>
            <tr>
                <th class="super" colspan="13">Weather for Jankytown, USA</th>
            </tr>
            <tr class="head">
                <th>Month</th>
                <?php getRow( $months, 1); ?>
            </tr>
            <tr>
                <th class="data">Average High &deg;F (&deg;C)</th>
                <?php getRow($avgH,2); ?>
            </tr>
            <tr>
                <th class="data">Daily Mean &deg;F (&deg;C)</th>
                <?php getRow( $mean,2); ?>
            </tr>
            <tr>
                <th class="data">Average Low &deg;F (&deg;C)</th>
                <?php getRow($avgL,2); ?>
            </tr>
            <tr>
                <th class="data">Precipitation in. (mm)</th>
                <?php getRow($prec, 3); ?>
            </tr>
            <tr class="footer">
                <td colspan="13" class="footer">
                    Strictly For Informational Purposes.
                    We cannot be held responsible for rain.
                </td>
            </tr>
        </table>
        <h2> Sales Table </h2>
        <table class="salesTable" >
            <tr>
                <th colspan="2">Zoo Sale</th>
            </tr>
            <tr>
                <td rowspan="3" class="redText">
                    Large animals on sale this week. All animals over 3 tons
                    just $45,000!
                </td>
                <td class="redAnimal">
                    <img src="images/elephant.gif" alt="elephant" />
                </td>
            </tr>
            <tr>
                <td class="redAnimal">
                    <img src="images/rhino.gif" alt="rhino" />
                </td>
            </tr>
            <tr>
                <td class="redAnimal">
                    <img src="images/lion.gif" alt="lion" />
                </td>
            </tr>
            <tr>
                <td class="greenText">
                    Zebras only $12,000!
                </td>
                <td class="greenAnimal">
                    <img src="images/zebra.gif" alt="zebra" />
                </td>
            </tr>
            <tr>
                <td class="blueText">
                    All giraffes, 20% off!
                </td>
                <td class="blueAnimal">
                    <img src="images/giraffe.gif" alt="giraffe" />
                </td>
            </tr>
        </table>

        <!-- THIS IS THE VALIDATION LINK THAT HOVERS IN THE LOWER LEFT-->
        <p style="position:fixed; top:5px; right:5px">
            <a href="http://validator.w3.org/check?uri=referer">
                <img style ="border:0;width:88px;height:31px"
                     src="http://www.w3.org/Icons/valid-xhtml10-blue"
                     alt="Valid XHTML 1.0 Transitional"/>
            </a>
        </p>   

        <!-- This is the menu for the gradients -->
        <?php include 'menu.php'; ?>
    </body>
</html>