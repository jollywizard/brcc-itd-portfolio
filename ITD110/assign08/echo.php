<?php
if (!isset($_GET['file'])) $fileName = "index.php";
else $fileName = $_GET['file'];
?>

<html>
    <head>
        <title><?php echo $fileName ?> used in Assignment 8 by James Arlow</title>
    </head>
    <body>
        <pre>
<?php
$file = file_get_contents($fileName, FILE_USE_INCLUDE_PATH);
echo htmlspecialchars ($file);
?>
        </pre>
    </body>
</html>