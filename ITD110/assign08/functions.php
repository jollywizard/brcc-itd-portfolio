<?php

/**
 * Variables that pertain to gradients are taken from get,
 * else they revert to default
 */
if ( !isset($_GET['base']) || $_GET['base'] < 0 || $_GET['base'] > 255)
    $base = 255;
else { $base = $_GET['base']; }

if ( !isset($_GET['mode']) ) $mode = 'blueTint';
else $mode = $_GET['mode'];

/**all data is stored in arrays so the tables can be procedurally generated.
 * In a realistic scenario this data would be fetched from a database.
 * !!! AND NOT FORCED UPON STUDENTS TO COPY BY HAND BECAUSE ITS EMBEDDED IN A PICTURE!!!
 * what kind of sick joke was that anyway.
 */
$months = array("Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
$avgH = array(63,64,70,73,72,70,68,68,68,66,66,63);
$mean = array(55,57,61,64,64,63,63,63,61,59,57,55);
$avgL = array(46,48,52,55,55,55,55,55,55,52,50,56);
$prec = array(0 ,0 ,.4 ,.8 ,2.8 ,6.3 ,5.5 ,5.5 ,5.9 ,2.4 ,.9 ,0);

//generates column tags for nth-child selectors if desired
//my original style info used said method before I realized it
//wasn't supported in IE.
function colums($input) {
    for ($i = 0; $i < $input; $i++) {
        echo "<col />";
    }
}

//Converts temperatures to celcious
function toCel ($input) {
    return round ((5/9) * ($input - 32));
}

//Converts inches to millimeters.
//I double checked the data, when mine showed discrepancies from yours.
//My data is correct according to several calculation sources.
function toMM ($input) {
    return round (25.4 * $input);
}

// the function used to generate the red scale for temperatures.
// Wasn't as adaptable as I like so I went all out for the blue below.
function getRed ($input) {
    $r = ($input/2 * $input/10);
    if ($r > 255 ) $r = "ff1010";
    else if($r < 0) $r = "f11";
        else $r = dechex($r)."1010";
    return $r;
}

/** I found that my red algorithm was insufficient for the blue problem
 * and set about to come up with an appropriate algorithm that could be normalized
 * across different data results.  This is that algorithm. You can see that
 * instead of hardcoding the hex values as with getRed(), each color is treated
 * independently as a variable.  Also, as explained below, I addressed the problem
 * of low values appearing black.
 *
 * It is still somewhat hardcoded to the current application, and truth be
 * told the results are a bit to similar for my tastes, everything close to the
 * max value looks totally blue.  This could be fixed, by snapping to web safe/smart
 * colors or taking the total data set and applying some standard deviation, which
 * would fix small disparate data sets such as the one we're currently working with.
 *
 * Despite the shortcommings, the approach is still a better example of how
 * to gradiate rgb colors than I have been able to find hunting the web because
 * instead of decreasing the color content and heading towards black which is
 * unreadable, low values keep their dominant color content and are
 * converted to lighter tints or shifted towards intermediate colors
 * depending on the configuration.
 *
 * With a little parsing and switch work on a parameter string this could
 * become an allpurpose algorithm for color gradients across data sets.
 *
 * Although, in its current form its limited to tints and color gradients, by setting
 * a mean value you could, say gradient to blue for values below a certain
 * temperature and to red for those above.  Said operation would actually be
 * easier in php than in most languages, because of the $$variableNameInAvariable
 * construct (which I made use of in the getFormTable function in menu.php).
 *
 * The first permutation would be to convert it to a function that could be called with:
 * getColorShade($modeString, $value, $max, $min = 0, $base = 255)
 * where modeString would take a form like 'g+r-b+' with the order indicating
 * the value heirarchy and the signs indicating the type of blending to occur.
 *
 * Perhaps if I hadn't spent so much time on this elementary task I'd be willing
 * to tackle the challenge now, but alas.
 *
 * Still at least I took the opportunity to learn and deduce, amirite.
 */
function getBlue ($input) {
    /* the central value, which defines tint of unmodified data.*/
    global $base;
    global $mode;
    $max = 6.3; //the maximum value which is used to normalize the results.

    $b = 255; //as the root color, the base is set to max.

    $normal = true;
    if(isset($_GET['mode'])) {
        switch ($_GET['mode']) {
        //for primary tints: one color is maxed and the other two decrease evenly
        //see default method below
            case 'blueTint': break;

            //for intermediate color tints: two values are set to max
            case 'magentaTint':
                $r = 255;
                $g = $base - ( $input / $max * $base); //( (percent) * base )
                $normal = false;
                break;
            case 'cyanTint':
                $g = 255;
                $r = $base - ( $input / $max * $base); //( (percent) * base )
                $normal = false;
                break;

            //for transitions, one color is ignored and the others are proportional
            case '2cyan':
                $g = $base - ( $input / $max * $base); //( (percent) * base )
                $r = 0;
                $normal = false;
                break;
            case '2magenta':
                $r = $base - ( $input / $max * $base); //( (percent) * base )
                $g = 0;
                $normal = false;
                break;

            /*the trasitions can be lightened by applying a ratio
             * such as $g = $r / 2; this increases the white factor
             * and washes out the dominant colors.
             */
        }
    }

    if ($normal) { //default blue tint
    /* sets g as a percentage of the base value by finding
     * finding the proportion of the input to the max value  */
        $g = $base - ( $input / $max * $base); //( (percent) * base )
        $r = $g;
    }

    /* The following block prevents numbers from going out of valid bounds and
     * converts the values in to hex, padding with 0's for single digit values */
    if ($r > 255) $r = 255; elseif ($r < 0) $r = 0;
    if ($g > 255) $g = 255; elseif ($g < 0) $g = 0;
    if ($b > 255) $b = 255; elseif ($b < 0) $b = 0;
    if ($r < 16) $r = "0".dechex($r); else $r = dechex($r);
    if ($g < 16) $g = "0".dechex($g); else $g = dechex($g);
    if ($b < 16) $b = "0".dechex($b); else $b = dechex($b);
    return $r.$g.$b; //return concatenated strings.
}

/* generates the data portion of a table row, with special case switches
 * for all headers (months), redscale data (temp),
 * and blue scale data (precipitation)
 *
 * options: 0 = plain; 1 = all headers; 2 = temp; 3 = precip*/

function getRow($array, $option = 0) {

    foreach ($array as $data) {
        if ($option == 1) {echo "\n\t<th>";}
        else {
            echo "\n\t<td";
            switch ($option) {
                case 2: $bg = getRed($data);
                    break;
                case 3: $bg = getBlue($data);
                    break;
            }
            if (isset($option)) echo ' style="background-color: #'.$bg.';"';
            echo ">";
        }
        echo $data;
        switch($option) {
            case 2: echo " (".toCel($data).") ";
                break;
            case 3: echo " (".toMM($data).") ";
                break;
        }
        if ($option == 1) echo "</th>";
        else echo "</td>";
    }
    echo "\n";
}

?>