We�ll take you for a ride!

At SuperLuxury Happy Time Travel, Inc., we care about the customer in near-appropriate amounts. Our only goal is to get you there and back with happy memories and lighter pockets. Located in beautiful Charlottesville, Virginia (go dawgs!), our offices are open seven days a week to help you plan your next trip in safety and style. 

In 2007 we are proud to offer trips to Mexico, Italy and Japan. Please take a look inside for more information.SuperLuxury Happy Time Travel - we�ll take you for a ride!

La dolce vita!Enjoy the sweet life in Florence, Italy, home of the renaissance. Great new travel itineraries at SuperLuxury Happy Time Travel, Inc. Mediocre tourist food and institutionalized rudeness is no excuse to miss the beautiful artwork of Florence!Why Florence?

Florence is the birthplace of the renaissance, where sculpture, painting and architecture were reborn in the Western world. 
Florence was the home of the Medici, the most powerful and influential family in Europe, who funded Michelangelo, Leonardo da Vinci and many other famous artists.
Armani, Gucci, Prada and dozens of other designer clothing stores can be found in walking distance of anywhere in the historic district.
Florence�s historic district is easy to walk; you can get almost anywhere within twenty minutes on foot.

When you get sick of being treated like a prisoner, you can catch a train to beautiful Cortona, in the Tuscan countryside; only an hour and a half ride through the country!

The town of Pisa, famous for its leaning tower, is less than an hour away by train.

You can eat a pigeon!

You can buy tripe sandwiches with salsa verde in the alley next to Dante�s house.

Stand in line for two hours to see the statue of David (it�s worth it!)

Shop for jewelry along the Ponte Vecchio.
Sleep in a room twice as old as the U.S.A. and have the mosquito bites to prove it.
Pigeon!

Tokyo Package
Spend 6 days and 7 nights in Tokyo, Japan, the most hi-tech city in the world! 

Although huge, Tokyo is safer than any big city in the world, and offers incredible shopping and eating. 

Four star accommodations plus round-trip airfare, $2,995 (per person, plus taxes)


Florence Package

Spend 4 days, 5 nights in the heart of the renaissance. 

Stroll along the River Arno, see Michelangelo�s David and eat horse meat without even realizing it! 

Three star accommodations plus round trip airfare, $1995 (per person, plus taxes)



San Miguel Package

Hang out with billionaires and broke artists and see why Michael Jordan calls it "that place where I have a house." San Miguel de Allende, Mexico is where everyone who�s anyone winds up before the end of the world. 

Four days, five nights at a five star luxury villa in town, plus roundtrip airfare, $3995 for two adults.



For custom travel packages, please contact us! We can help you create an itinerary that meets your unique needs. Enjoy San Miguel de Allende, Mexico, where spring is year round! A haven for bored retirees and rich movie stars alike, San Miguel de Allende, Mexico is a beautiful colonial town in the mountains, three hours north of Mexico City. Enjoy shopping and fine dining, or take a trip t the nearby botanical gardens. Surrounding towns feature great deals on blown glass, ceramics and leather goods. San Miguel is also an excellent place to buy silver jewelry for your friends back home.Many people who have visited here have wound up permanent or semi-permanent residents!