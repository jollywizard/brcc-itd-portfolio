<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>JiMinitaur Integration</title>
        <link href="styles.css" rel="stylesheet" media="screen" type="text/css" />
    </head>
    <body>
        <!-- Positioned outside the content so it matches page width-->
        <div id="header">
            <div id="labyrinth" >
                <div id="company">
                    <div id="head" ></div>
                    JiMinitaur Integration
                    <div id="axe"></div>
                </div>
            </div>
        </div>
        <div id="lower">
            <!-- The beggining of the menu;-->
            <div id="sidebar">
                <div id="menu">
                    <ul class="menu">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <span>Business</span>
                            <div class="subMenu">
                                <a href="services.html">Services</a>
                                <a href="rates.html">Rates</a>
                                <a href="form.html">Inquiries</a>
                            </div>
                        </li>
                        <li>
                            <a href="java.html">Java</a>
                            <div class="subMenu">
                                <a href="selftyping.html">Recursive&nbsp;Types</a>
                            </div>
                        </li>
                    </ul> <!-- END menu list -->
                </div> <!-- END <div id="menu">-->
            </div><!-- END <div id="sidebar">-->

            <!-- The page dive -->
            <div id="page">
                <div id="overflow">
                    <div id="content">
                        <div id="body">
                            <div>
                                <div id="userland">

                                    <h1> Thanks for the Submission </h1>
                                    <p>
                                        <?
                                        //DO NOT CHANGE ANYTHING WITHIN THIS BLOCK OF CODE
                                        echo("Thank you for your information!" );

                                        ?>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>